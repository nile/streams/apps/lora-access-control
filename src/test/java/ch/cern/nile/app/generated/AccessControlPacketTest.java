package ch.cern.nile.app.generated;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Map;

import com.google.gson.JsonElement;

import org.junit.jupiter.api.Test;

import ch.cern.nile.common.exceptions.DecodingException;
import ch.cern.nile.kaitai.decoder.KaitaiPacketDecoder;
import ch.cern.nile.test.utils.TestUtils;

public class AccessControlPacketTest {

    private static final JsonElement DATA_FRAME = TestUtils.getDataAsJsonElement("QCIAAQAAAAAAAA4=");
    private static final JsonElement KEEP_ALIVE_FRAME = TestUtils.getDataAsJsonElement("MKAA+AAAAAAAAAI=");
    private static final JsonElement UNSUPPORTED_FRAME = TestUtils.getDataAsJsonElement("EAAhwAAAQxMBAA==");

    @Test
    void givenDataFrame_whenDecoding_thenCorrectlyDecodesMessageData() throws DecodingException {
        Map<String, Object> packet = KaitaiPacketDecoder.decode(DATA_FRAME, AccessControlPacket.class);

        assertEquals(1L, packet.get("FRAMECOUNTER"));
        assertFalse((boolean) packet.get("CONFIG"));
        assertTrue((boolean) packet.get("LOWBAT"));
        assertEquals(1, packet.get("CHANNEL1INFO"));
        assertEquals(0, packet.get("CHANNEL2INFO"));
        assertEquals(0, packet.get("CHANNEL3INFO"));
        assertEquals(0, packet.get("CHANNEL4INFO"));
        assertFalse((boolean) packet.get("CHANNEL1CURRENTSTATE"));
        assertTrue((boolean) packet.get("CHANNEL2CURRENTSTATE"));
        assertFalse((boolean) packet.get("CHANNEL3CURRENTSTATE"));
        assertFalse((boolean) packet.get("CHANNEL4CURRENTSTATE"));
        assertTrue((boolean) packet.get("CHANNEL1PREVIOUSSTATE"));
        assertTrue((boolean) packet.get("CHANNEL2PREVIOUSSTATE"));
        assertFalse((boolean) packet.get("CHANNEL3PREVIOUSSTATE"));
        assertFalse((boolean) packet.get("CHANNEL4PREVIOUSSTATE"));
        assertNull(packet.get("TIMESTAMP"));
    }

    @Test
    void givenKeepAliveFrame_whenDecoding_thenCorrectlyDecodesMessageData() throws DecodingException {
        Map<String, Object> packet = KaitaiPacketDecoder.decode(KEEP_ALIVE_FRAME, AccessControlPacket.class);

        assertEquals(5L, packet.get("FRAMECOUNTER"));
        assertFalse((boolean) packet.get("CONFIG"));
        assertFalse((boolean) packet.get("LOWBAT"));
        assertEquals(248, packet.get("CHANNEL1INFO"));
        assertEquals(0, packet.get("CHANNEL2INFO"));
        assertEquals(0, packet.get("CHANNEL3INFO"));
        assertEquals(0, packet.get("CHANNEL4INFO"));
        assertFalse((boolean) packet.get("CHANNEL1CURRENTSTATE"));
        assertTrue((boolean) packet.get("CHANNEL2CURRENTSTATE"));
        assertFalse((boolean) packet.get("CHANNEL3CURRENTSTATE"));
        assertFalse((boolean) packet.get("CHANNEL4CURRENTSTATE"));
        assertNull(packet.get("TIMESTAMP"));
    }

    @Test
    void givenUnsupportedFrame_whenDecoding_thenThrowsDecodingException() {
        assertThrows(DecodingException.class,
                () -> KaitaiPacketDecoder.decode(UNSUPPORTED_FRAME, AccessControlPacket.class));
    }

}
