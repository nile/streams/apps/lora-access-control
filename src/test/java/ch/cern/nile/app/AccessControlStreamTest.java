package ch.cern.nile.app;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.google.gson.JsonObject;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.Test;

import ch.cern.nile.test.utils.StreamTestBase;
import ch.cern.nile.test.utils.TestUtils;

public class AccessControlStreamTest extends StreamTestBase {

    private static final JsonObject DATA_FRAME = TestUtils.loadRecordAsJson("data/data_frame.json");
    private static final JsonObject KEEP_ALIVE_FRAME = TestUtils.loadRecordAsJson("data/keep_alive_frame.json");
    private static final JsonObject UNSUPPORTED_FRAME = TestUtils.loadRecordAsJson("data/unsupported_frame.json");

    @Override
    public AccessControlStream createStreamInstance() {
        return new AccessControlStream();
    }

    @Test
    void givenDataFrame_whenDecoding_thenCorrectOutputRecordIsCreated() {
        pipeRecord(DATA_FRAME);
        final ProducerRecord<String, JsonObject> outputRecord = readRecord();

        JsonObject payload = outputRecord.value().getAsJsonObject("payload");

        assertEquals("0018b21000006a53", payload.get("DEVEUI").getAsString());

        assertEquals("0", payload.get("FRAMECOUNTER").getAsString());
        assertFalse(payload.get("CONFIG").getAsBoolean());
        assertFalse(payload.get("LOWBAT").getAsBoolean());
        assertEquals("2", payload.get("CHANNEL1INFO").getAsString());
        assertEquals("0", payload.get("CHANNEL2INFO").getAsString());
        assertEquals("0", payload.get("CHANNEL3INFO").getAsString());
        assertEquals("0", payload.get("CHANNEL4INFO").getAsString());
        assertEquals("QAAAAgAAAAAAAA8=", payload.get("DATA").getAsString());
        assertEquals("DataFrame", payload.get("FRAMETYPE").getAsString());
        assertTrue(payload.get("CHANNEL1CURRENTSTATE").getAsBoolean());
        assertTrue(payload.get("CHANNEL2CURRENTSTATE").getAsBoolean());
        assertFalse(payload.get("CHANNEL3CURRENTSTATE").getAsBoolean());
        assertFalse(payload.get("CHANNEL4CURRENTSTATE").getAsBoolean());
        assertTrue(payload.get("CHANNEL1PREVIOUSSTATE").getAsBoolean());
        assertTrue(payload.get("CHANNEL2PREVIOUSSTATE").getAsBoolean());
        assertFalse(payload.get("CHANNEL3PREVIOUSSTATE").getAsBoolean());
        assertFalse(payload.get("CHANNEL4PREVIOUSSTATE").getAsBoolean());
        assertNotNull(payload.get("TIMESTAMP"), "Timestamp is null");
    }


    @Test
    void givenKeepAliveFrame_whenDecoding_thenCorrectOutputRecordIsCreated() {
        pipeRecord(KEEP_ALIVE_FRAME);
        final ProducerRecord<String, JsonObject> outputRecord = readRecord();

        JsonObject payload = outputRecord.value().getAsJsonObject("payload");

        assertFalse(payload.get("CONFIG").getAsBoolean());
        assertFalse(payload.get("LOWBAT").getAsBoolean());
        assertEquals(248, payload.get("CHANNEL1INFO").getAsInt());
        assertEquals(0, payload.get("CHANNEL2INFO").getAsInt());
        assertEquals(0, payload.get("CHANNEL3INFO").getAsInt());
        assertEquals(0, payload.get("CHANNEL4INFO").getAsInt());
        assertEquals("MKAA+AAAAAAAAAI=", payload.get("DATA").getAsString());
        assertEquals("KeepAliveFrame", payload.get("FRAMETYPE").getAsString());
        assertFalse(payload.get("CHANNEL1CURRENTSTATE").getAsBoolean());
        assertTrue(payload.get("CHANNEL2CURRENTSTATE").getAsBoolean());
        assertFalse(payload.get("CHANNEL3CURRENTSTATE").getAsBoolean());
        assertFalse(payload.get("CHANNEL4CURRENTSTATE").getAsBoolean());
        assertNotNull(payload.get("TIMESTAMP"), "Timestamp is null");
    }

    @Test
    void givenUnsupportedFrame_whenDecoding_thenIgnoresFrameAndProcessesNextCorrectData() {
        pipeRecord(UNSUPPORTED_FRAME);
        pipeRecord(KEEP_ALIVE_FRAME);
        final ProducerRecord<String, JsonObject> outputRecord = readRecord();
        JsonObject payload = outputRecord.value().getAsJsonObject("payload");
        assertFalse(payload.get("CONFIG").getAsBoolean());
    }

}
